package com.atcstudy.dao;

import com.atcstudy.dao.mapping.UserRowMapper;
import com.atcstudy.jdo.User;
import com.atcstudy.utils.FileUtils;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UsersDaoImpl implements UsersDao{
    private static final Logger log = LoggerFactory.getLogger(UsersDaoImpl.class);

    private static final String CREATE_USERS_TABLE = FileUtils.loadScriptFromFile(
            "/sql/create/users.sql");
    private static final String GET_USERS = FileUtils.loadScriptFromFile(
            "/sql/get/get-users.sql");
    private static final String GET_USERS_ACTIVE = FileUtils.loadScriptFromFile(
            "/sql/get/get-users-active.sql");
    private static final String GET_USER_BY_EMAIL = FileUtils.loadScriptFromFile(
            "/sql/get/get-user-by-email.sql");
    private static final String UPDATE_USER = FileUtils.loadScriptFromFile(
            "/sql/update/user-update.sql" );
    private static final String INSERT_USER = FileUtils.loadScriptFromFile(
            "/sql/insert/user-insert.sql" );

    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    NamedParameterJdbcTemplate namedJdbc;

    @PostConstruct
    public void init()
    {
        createUsersTable();
    }

    @Override
    @Transactional
    public void createUsersTable() {
        log.info("creating USERS table");
        jdbcTemplate.execute(CREATE_USERS_TABLE);
    }

    @Override
    @Transactional
    public List<User> getUsers() {
        log.info("calling getUsers");
        Map<String, Object> params = new HashMap<>();

        return namedJdbc.query(GET_USERS, params, new UserRowMapper());
    }

    @Override
    @Transactional
    public List<User> getUsersActive() {
        log.info("calling getUsersActive");
        Map<String, Object> params = new HashMap<>();

        return namedJdbc.query(GET_USERS_ACTIVE, params, new UserRowMapper());
    }

    @Override
    @Transactional
    public User getUserByEmail(String email) {
        log.info("calling getUserByEmail");
        Map<String, Object> params = new HashMap<>();
        params.put("email", email);

        return namedJdbc.queryForObject(GET_USER_BY_EMAIL, params, new UserRowMapper());
    }

    @Override
    @Transactional
    public void saveUser(User user)
    {
        log.info("calling saveUser");
        Map<String, Object> params = new HashMap<>();
        params.put("userId", user.getUserId());
        params.put("firstName", user.getFirstName());
        params.put("lastName", user.getLastName());
        params.put("email", user.getEmail());
        params.put("password", user.getPassword());
        params.put("isActive", user.isIsActive());
        params.put("birthday", user.getBirthday());


        namedJdbc.update(INSERT_USER, params);

    }

    @Override
    @Transactional
    public void updateUser(User user)
    {
        log.info("calling updateUser");
        Map<String, Object> params = new HashMap<>();
        params.put("userId", user.getUserId());
        params.put("firstName", user.getFirstName());
        params.put("lastName", user.getLastName());
        params.put("email", user.getEmail());
        params.put("password", user.getPassword());
        params.put("isActive", user.isIsActive());
        params.put("birthday", user.getBirthday());

        namedJdbc.update(UPDATE_USER, params);
    }
}
