package com.atcstudy.dao;

import com.atcstudy.dao.mapping.OrderRowMapper;
import com.atcstudy.jdo.Order;
import com.atcstudy.utils.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OrdersDaoImpl implements OrdersDao {
    private static final Logger log = LoggerFactory.getLogger(OrdersDaoImpl.class);

    private static final String CREATE_ORDERS_TABLE = FileUtils.loadScriptFromFile(
            "/sql/create/orders.sql");
    private static final String GET_ORDERS = FileUtils.loadScriptFromFile(
            "/sql/get/get-orders.sql");
    private static final String GET_ORDERS_BY_TOUR = FileUtils.loadScriptFromFile(
            "/sql/get/get-orders-by-tour.sql");
    private static final String GET_ORDERS_BY_USER = FileUtils.loadScriptFromFile(
            "/sql/get/get-orders-by-user.sql");

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedJdbc;

    @PostConstruct
    public void init()
    {
        log.info("creating ORDERS table");
        createOrdersTable();
    }

    @Override
    @Transactional
    public void createOrdersTable() {
        log.info("creating ORDERS table");
        jdbcTemplate.execute(CREATE_ORDERS_TABLE);
    }

    @Override
    @Transactional
    public List<Order> getOrders() {
        log.info("calling getOrders");
        Map<String, Object> params = new HashMap<>();

        return namedJdbc.query(GET_ORDERS, params, new OrderRowMapper());
    }

    @Override
    @Transactional
    public List<Order> getOrdersByTourId(Long tourId) {
        log.info("calling getOrdersByTourId");
        Map<String, Object> params = new HashMap<>();
        params.put("tourId", tourId);

        return namedJdbc.query(GET_ORDERS_BY_TOUR, params, new OrderRowMapper());
    }

    @Override
    @Transactional
    public List<Order> getOrdersByUserId(Long userId) {
        log.info("calling getOrdersByUserId");
        Map<String, Object> params = new HashMap<>();
        params.put("userId", userId);

        return namedJdbc.query(GET_ORDERS_BY_USER, params, new OrderRowMapper());
    }
}
