package com.atcstudy.dao;

import com.atcstudy.jdo.Order;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface OrdersDao {

    @Transactional
    void createOrdersTable();

    @Transactional
    List<Order> getOrders();

    @Transactional
    List<Order> getOrdersByUserId(Long userId);

    @Transactional
    List<Order> getOrdersByTourId(Long tourId);
}
