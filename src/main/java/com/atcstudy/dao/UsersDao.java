package com.atcstudy.dao;

import com.atcstudy.jdo.User;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface UsersDao {

    @Transactional
    void createUsersTable();

    @Transactional
    List<User> getUsers();

    @Transactional
    List<User> getUsersActive();

    @Transactional
    User getUserByEmail(String email);

    @Transactional
    void saveUser(User user);

    @Transactional
    void updateUser(User user);
}
