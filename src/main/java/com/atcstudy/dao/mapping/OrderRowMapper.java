package com.atcstudy.dao.mapping;

import com.atcstudy.jdo.Order;
import com.atcstudy.jdo.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class OrderRowMapper implements RowMapper<Order> {
    @Override
    public Order mapRow(ResultSet resultSet, int i) throws SQLException {
        Order order = new Order();
        order.setOrderId(resultSet.getLong("orderId"));
        order.setConfirmed(resultSet.getBoolean("isConfirmed"));
        order.setTimeKey(resultSet.getObject("timeKey", LocalDateTime.class));

        //setting user
        User user = new User();
        user.setUserId(resultSet.getLong("userId"));
        user.setFirstName(resultSet.getString("firstName"));
        user.setLastName(resultSet.getString("lastName"));
        user.setEmail(resultSet.getString("email"));
        user.setPassword(resultSet.getString("password"));
        user.setIsActive(resultSet.getBoolean("isActive"));
        user.setBirthday(resultSet.getObject("birthday", LocalDate.class));
        order.setUser(user);

        return order;
    }
}
