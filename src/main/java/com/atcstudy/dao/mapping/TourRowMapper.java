package com.atcstudy.dao.mapping;

import com.atcstudy.jdo.Tour;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

public class TourRowMapper implements RowMapper<Tour> {
    @Override
    public Tour mapRow(ResultSet resultSet, int i) throws SQLException {
        Tour tour = new Tour();
        tour.setTourId(resultSet.getLong("tourId"));
        tour.setTourName(resultSet.getString("tourName"));
        tour.setTourDescription(resultSet.getString("tourDescription"));
        tour.setLocation(resultSet.getString("location"));
        tour.setStartDate(resultSet.getObject("startDate", LocalDate.class));
        tour.setEndDate(resultSet.getObject("endDate", LocalDate.class));
        tour.setCountLimit(resultSet.getInt("countLimit"));

        return tour;
    }
}
