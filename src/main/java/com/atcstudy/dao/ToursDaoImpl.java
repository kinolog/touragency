package com.atcstudy.dao;

import com.atcstudy.dao.mapping.TourRowMapper;
import com.atcstudy.jdo.Tour;
import com.atcstudy.utils.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ToursDaoImpl implements ToursDao {
    private static final Logger log = LoggerFactory.getLogger(ToursDaoImpl.class);

    private static final String CREATE_TOURS_TABLE = FileUtils.loadScriptFromFile(
            "/sql/create/tours.sql");
    private static final String GET_TOURS = FileUtils.loadScriptFromFile(
            "/sql/get/get-tours.sql");
    private static final String GET_TOUR_BY_NAME = FileUtils.loadScriptFromFile(
            "/sql/get/get-tour-by-name.sql");

    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    NamedParameterJdbcTemplate namedJdbc;

    @PostConstruct
    public void init()
    {
        log.info("creating TOURS table");
        createToursTable();
    }

    @Override
    @Transactional
    public void createToursTable() {
        jdbcTemplate.execute(CREATE_TOURS_TABLE);
    }

    @Override
    @Transactional
    public List<Tour> getTours() {
        log.info("calling getTours");
        Map<String, Object> params = new HashMap<>();

        return namedJdbc.query(GET_TOURS, params, new TourRowMapper());
    }

    @Override
    @Transactional
    public Tour getTourByName(String tourName) {
        log.info("calling getTourByName");
        Map<String, Object> params = new HashMap<>();
        params.put("tourName", tourName);

        return namedJdbc.queryForObject(GET_TOUR_BY_NAME, params, new TourRowMapper());
    }
}
