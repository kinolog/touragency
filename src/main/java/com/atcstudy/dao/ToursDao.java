package com.atcstudy.dao;

import com.atcstudy.jdo.Tour;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ToursDao {

    @Transactional
    void createToursTable();

    @Transactional
    List<Tour> getTours();

    @Transactional
    Tour getTourByName(String tourName);

}
