package com.atcstudy.controller;

import com.atcstudy.jdo.User;
import com.atcstudy.service.AuthenticationService;
import com.atcstudy.service.UsersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@Controller
public class LoginController {
    private static final Logger log = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    private UsersService usersService;

    @Autowired
    private AuthenticationService authenticationService;

    @RequestMapping(value = "/perform_login", method = RequestMethod.POST)
    @ResponseBody
    public User perform_login(@RequestBody User user) {
        System.out.println(user.getEmail());
        User loginResult = authenticationService.loginUser(user.getEmail(), user.getPassword());
        return loginResult;
    }

    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public void logout()
    {
        authenticationService.logoutUser();
    }


}
