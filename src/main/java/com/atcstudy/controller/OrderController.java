package com.atcstudy.controller;

import com.atcstudy.jdo.Order;
import com.atcstudy.service.OrdersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.List;

@CrossOrigin
@Controller
public class OrderController {
    private static final Logger log = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    private OrdersService ordersService;

    @GetMapping(value = "/orders")
    @ResponseBody
    public List<Order> getOrders(HttpServletRequest request,
                                 HttpServletResponse response)
    {
        try {
            log.info("getOrders has been called");

            return ordersService.getOrders();
        }
        catch (Exception e)
        {
            log.error(e.getMessage());
            return Collections.emptyList();
        }
    }

    @GetMapping(value = "/orders/byTourId/{tourId}")
    @ResponseBody
    public List<Order> getOrdersByTourId(@PathVariable Long tourId)
    {
        try {
            log.info("getOrdersByTourId has been called");

            return ordersService.getOrdersByTourId(tourId);
        }
        catch (Exception e)
        {
            log.error(e.getMessage());
            return Collections.emptyList();
        }
    }

    @GetMapping(value = "/orders/byUserId/{userId}")
    @ResponseBody
    public List<Order> getOrdersByUserId(@PathVariable Long userId)
    {
        try {
            log.info("getOrdersByUserId has been called");

            List<Order> result = ordersService.getOrdersByUserId(userId);
            System.out.println(result.get(0));
            return ordersService.getOrdersByUserId(userId);
        }
        catch (Exception e)
        {
            log.error(e.getMessage());
            return Collections.emptyList();
        }
    }
}
