package com.atcstudy.controller;

import com.atcstudy.jdo.Tour;
import com.atcstudy.service.ToursService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.List;

@Controller
public class TourController {
    private static final Logger log = LoggerFactory.getLogger(TourController.class);

    @Autowired
    private ToursService toursService;

    @GetMapping(value = "/tours")
    public String getTours(ModelMap model)
    {
        model.addAttribute("tours", toursService.getTours());
        return "toursList";
    }

    @GetMapping(value = "/tours/{tourName}")
    public String getTourByName(@PathVariable String tourName, ModelMap model)
    {
        model.addAttribute("tour", toursService.getTourByName(tourName));
        return "tourInfo";
    }

}
