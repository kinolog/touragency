package com.atcstudy.controller;

import com.atcstudy.jdo.User;
import com.atcstudy.service.AuthenticationService;
import com.atcstudy.service.UsersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@CrossOrigin
@Controller
public class UserController {
    private static final Logger log = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UsersService usersService;

    @Autowired
    private AuthenticationService authenticationService;

    @GetMapping(value = "/users")
    public String getUsers(ModelMap model)
    {
        log.info("getting Thymeleaf Users");
        model.addAttribute("users", usersService.getUsers());
        return "usersList";
    }

    @GetMapping(value = "/usersActive")
    public String getUsersActive(ModelMap model)
    {
        model.addAttribute("users", usersService.getUsersActive());
        return "usersList";
    }

    @GetMapping(value = "/users/byEmail/{email}")
    public User getUserByEmail(@PathVariable String email)
    {
        return usersService.getUserByEmail(email);
    }

    @RequestMapping(value = "home/profile", method = RequestMethod.GET)
    @ResponseBody
    public User userInfo()
    {
        return authenticationService.getCurrentUser();
    }
}
