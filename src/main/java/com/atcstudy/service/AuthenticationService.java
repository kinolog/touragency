package com.atcstudy.service;

import com.atcstudy.jdo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationService {

    @Autowired
    private UsersService usersService;

    private User currentUser = null;

    public User loginUser(String username, String password)
    {
        User user = usersService.getUserByEmail(username);
        if (user.getPassword().equals(password)) {
            currentUser = user;
            return user;
        }
        return null;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void logoutUser()
    {
        currentUser = null;
    }
}
