package com.atcstudy.service;

import com.atcstudy.dao.ToursDao;
import com.atcstudy.jdo.Tour;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ToursService {
    private static final Logger log = LoggerFactory.getLogger(ToursService.class);

    @Autowired
    ToursDao toursDao;

    @Transactional
    public List<Tour> getTours()
    {
        log.info("getTours has been called");

        return toursDao.getTours();
    }

    @Transactional
    public Tour getTourByName(String tourName)
    {
        log.info("getTourByName has been called");

        return toursDao.getTourByName(tourName);
    }
}
