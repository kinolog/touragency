package com.atcstudy.service;

import com.atcstudy.dao.OrdersDao;
import com.atcstudy.jdo.Order;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class OrdersService {
    private static final Logger log = LoggerFactory.getLogger(OrdersService.class);

    @Autowired
    private OrdersDao ordersDao;

    @Transactional
    public List<Order> getOrders()
    {
        log.info("getOrders has been called");

        return ordersDao.getOrders();
    }

    @Transactional
    public List<Order> getOrdersByTourId(Long tourId)
    {
        log.info("getOrdersByTourId has been called");

        return ordersDao.getOrdersByTourId(tourId);
    }

    @Transactional
    public List<Order> getOrdersByUserId(Long userId)
    {
        log.info("getOrdersByUserId has been called");
        List<Order> orders = ordersDao.getOrdersByUserId(userId);
        System.out.println(orders);
        return ordersDao.getOrdersByUserId(userId);
    }
}
