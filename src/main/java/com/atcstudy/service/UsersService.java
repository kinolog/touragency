package com.atcstudy.service;

import com.atcstudy.dao.UsersDao;
import com.atcstudy.jdo.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UsersService {
    private static final Logger log = LoggerFactory.getLogger(UsersService.class);

    @Autowired
    private UsersDao usersDao;

    @Transactional
    public List<User> getUsers()
    {
        log.info("getUsers has been called");
        return usersDao.getUsers();
    }

    @Transactional
    public List<User> getUsersActive()
    {
        log.info("getUsersActive has been called");
        return usersDao.getUsersActive();
    }

    @Transactional
    public User getUserByEmail(String email)
    {
        log.info("getUserByEmail has been called");
        return usersDao.getUserByEmail(email);
    }

    @Transactional
    public void saveUser(User user)
    {
        usersDao.saveUser(user);
    }

    @Transactional
    public void updateUser(User user)
    {
        usersDao.updateUser(user);
    }
}
