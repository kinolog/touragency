package com.atcstudy.jdo;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name="users")
public class User {

    @Id
    @Column(name="userId")
    private long userId;
    private String firstName;
    private String lastName;
    private String email;

    @Column(name="password", length = 60)
    private String password;
    private boolean isActive;
    private LocalDate birthday;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    //@JoinColumn(name = "userId")
    private List<Order> orders;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(boolean active) {
        isActive = active;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    @Override
    public boolean equals(Object obj) {
        if (this==obj)
            return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        User that = (User)obj;

        if (!that.firstName.equals(firstName)) return false;
        if (!that.lastName.equals(lastName)) return false;
        if (!that.email.equals(email)) return false;
        if (!that.password.equals(password)) return false;
        if (that.isActive != isActive) return false;
        if (!that.birthday.equals(birthday)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int)userId;
        result = 31*result + firstName.hashCode();
        result = 31*result + lastName.hashCode();
        result = 31*result + email.hashCode();
        result = 31*result + password.hashCode();
        result = 31*result + birthday.hashCode();

        return result;
    }
}
