package com.atcstudy.jdo;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name="orders")
public class Order {
    @Id
    @Column(name="orderId")
    private Long orderId;


    @MapsId("userId")
    @ManyToOne(optional = false)
    @JoinColumn(name = "userId", referencedColumnName = "userId", nullable = false)
    private User user;

    @MapsId("tourId")
    @ManyToOne(optional = false)
    @JoinColumn(name = "tourId", referencedColumnName = "tourId")
    private Tour tour;

    private boolean isConfirmed;
    private LocalDateTime timeKey;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public boolean isConfirmed() {
        return isConfirmed;
    }

    public void setConfirmed(boolean confirmed) {
        isConfirmed = confirmed;
    }

    public LocalDateTime getTimeKey() {
        return timeKey;
    }

    public void setTimeKey(LocalDateTime timeKey) {
        this.timeKey = timeKey;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Tour getTour() {
        return tour;
    }

    public void setTour(Tour tour) {
        this.tour = tour;
    }

    @Override
    public String toString() {
        return "Order: " + orderId.toString() + "; user: " +
                (user==null?"":user.getEmail());
    }

    @Override
    public boolean equals(Object obj) {
        if (this==obj)
            return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        Order that = (Order)obj;

        if (that.timeKey != timeKey) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return timeKey.hashCode();
    }
}
