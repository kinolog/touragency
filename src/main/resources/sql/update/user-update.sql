UPDATE ta.users
    SET firstName = :firstName,
        lastName = :lastName,
        email = :email,
        password = :password,
        isActive = :isActive,
        birthday = :birthday
WHERE userId = :userId;