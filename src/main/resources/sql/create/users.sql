CREATE SCHEMA IF NOT EXISTS ta;

DROP TABLE IF EXISTS ta.users;

CREATE TABLE IF NOT EXISTS ta.users (
  userId BIGINT,
  firstName TEXT,
  lastName TEXT,
  email TEXT,
  password TEXT,
  isActive BOOLEAN,
  birthday DATE
);

CREATE SEQUENCE IF NOT EXISTS userIdSequence START WITH 10;

INSERT INTO ta.users (userId, firstName, lastName, email, password, isActive, birthday)
  VALUES (11, 'A_name', 'A_surname', 'a@mail.ru', 'a_password',
           FALSE, PARSEDATETIME('1990-10-10', 'yyyy-mm-dd') );

INSERT INTO ta.users (userId, firstName, lastName, email, password, isActive, birthday)
  VALUES (12, 'B_name', 'B_surname', 'b@mail.ru', 'b_password',
          TRUE, PARSEDATETIME('1990-10-10', 'yyyy-mm-dd') );