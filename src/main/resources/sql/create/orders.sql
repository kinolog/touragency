CREATE SCHEMA IF NOT EXISTS ta;

DROP TABLE IF EXISTS ta.orders;

CREATE TABLE IF NOT EXISTS ta.orders (
  orderId BIGINT,
  userId BIGINT,
  tourId BIGINT,
  isConfirmed BOOLEAN,
  timeKey DATETIME
);

CREATE SEQUENCE IF NOT EXISTS orderIdSequence START WITH 10;

INSERT INTO ta.orders (orderId, userId, tourId, isConfirmed, timeKey)
  VALUES (11, 11, 11, TRUE, parsedatetime('2017-09-09 18:30:05', 'yyyy-mm-dd hh:mm:ss') );

INSERT INTO ta.orders (orderId, userId, tourId, isConfirmed, timeKey)
  VALUES (12, 12, 12, FALSE, parsedatetime('2017-09-09 18:30:06', 'yyyy-mm-dd hh:mm:ss') );