CREATE SCHEMA IF NOT EXISTS ta;

DROP TABLE IF EXISTS ta.tours;

CREATE TABLE IF NOT EXISTS ta.tours (
  tourId BIGINT,
  tourName TEXT,
  tourDescription TEXT,
  location TEXT,
  startDate DATE,
  endDate DATE,
  countLimit INT
);

CREATE SEQUENCE IF NOT EXISTS tourIdSequence START WITH 10;

INSERT INTO ta.tours (tourId, tourName, tourDescription, location, startDate, endDate, countLimit)
VALUES (11, 'tour_A', 'tour_A_Description', 'location_1', PARSEDATETIME('2017-10-10', 'yyyy-mm-dd'),
        parsedatetime('2017-11-11', 'yyyy-mm-dd'), 9);

INSERT INTO ta.tours (tourId, tourName, tourDescription, location, startDate, endDate, countLimit)
VALUES (12, 'tour_B', 'tour_B_Description', 'location_2', PARSEDATETIME('2017-10-09', 'yyyy-mm-dd'),
        parsedatetime('2017-11-09', 'yyyy-mm-dd'), 9);