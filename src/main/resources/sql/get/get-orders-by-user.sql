SELECT orderId AS orderId,
       tourId AS tourId,
       ta.orders.userId AS userId,
       isConfirmed AS isConfirmed,
       timeKey AS timeKey,
       firstName AS firstName,
       lastName AS lastName,
       email AS email,
       password AS password,
       isActive AS isActive,
       birthday AS birthday
FROM ta.orders, ta.users
WHERE ta.orders.userId = :userId AND ta.users.userId = :userId;