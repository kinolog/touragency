SELECT orderId AS orderId,
        tourId AS tourId,
        userId AS userId,
        isConfirmed AS isConfirmed,
        timeKey AS timeKey
FROM ta.orders;