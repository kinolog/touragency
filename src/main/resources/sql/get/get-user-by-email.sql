SELECT userId AS userId,
       firstname AS firstName,
       lastName AS lastName,
       email AS email,
       password AS password,
       isActive AS isActive,
       birthday AS birthday
FROM ta.users
WHERE email IS :email;