SELECT tourId AS tourId,
       tourName AS tourName,
       tourDescription AS tourDescription,
       location AS location,
       startDate AS startDate,
       endDate AS endDate,
       countLimit AS countLimit
FROM ta.tours
WHERE tourName IS :tourName;